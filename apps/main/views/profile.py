from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, HttpResponse
from django.views.decorators.http import require_POST
from django.shortcuts import render

from apps.main.forms import UserProfileForm
from apps.main.models import UserProfile

def view_profile(request, profile_id):
    profile = get_object_or_404(UserProfile, pk=profile_id)
    return render(request, 'profiles/profile.html', {'profile':profile})

@login_required
def update_profile(request):
    profile = get_object_or_404(UserProfile, pk=request.user.get_profile().pk)
    if request.method == 'GET':
        return render(request, 'profiles/form.html', {'form':UserProfileForm(instance=profile),})
    if request.method == 'POST':
        form = UserProfileForm(request.POST, request.FILES, instance=profile)
        if form.is_valid():
            form.save()
    return HttpResponseRedirect(reverse(view_profile, kwargs={'profile_id':profile.pk}))

@login_required
@require_POST
def update_avatar(request):
    profile = get_object_or_404(UserProfile, pk=request.user.get_profile().pk)
    if request.FILES:
        profile.photo = request.FILES['avatar']
        profile.save()
        return HttpResponse(profile.photo.url, status=200)
    if request.POST.get('clear_avatar', None):
        profile.photo = None
        profile.save()
        return HttpResponse('static/img/default_user.png', status=200)
