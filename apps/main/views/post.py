from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import HttpResponse, get_object_or_404
from django.views.decorators.http import require_http_methods
from django.shortcuts import render

from apps.main.forms import PostForm
from apps.main.models import Post

@login_required
def post_form(request, post_id=None):
    if request.method == 'POST':

        data = request.POST.copy()
        post = PostForm(request.POST)

        if post_id:
            post.instance = Post.objects.get(id=post_id)

        if  post.is_valid():
            post = post.save(commit=False)
            post.author = request.user
            if data['trip_mode'] == '0':
                for item in ['mon', 'tue', 'wed', 'thu', 'fri', 'sun', 'sat']:
                    if item in data:
                        post.regular_days += item+' '
            post.save()
            return HttpResponseRedirect(reverse(all_posts)+'?filter=personal')
        else:
            ctx = {'form':post}
            return render(request, 'posts/form.html', ctx)    

    if request.method == 'GET':
        ctx = {}
        if post_id:
            post = Post.objects.get(pk=post_id)
            start = post.start
            finish = post.end
            form = PostForm(instance=post)
            ctx_extra = {'start':start, 'finish':finish,}
            ctx.update(ctx_extra)
        else:
            init_data={'author':request.user}
            previous_posts = Post.objects.filter(author=request.user,search_target=1)
            if previous_posts:
                init_data['car_info'] = previous_posts[0].car_info
            form = PostForm(initial=init_data)
        
        ctx['form'] = form
        return render(request, 'posts/form.html', ctx)

@login_required
@require_http_methods(["DELETE"])
def delete_post(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    if post.author == request.user:
        post.delete()
        return HttpResponse('Post deleted')
    return HttpResponse('')

def all_posts(request):
    posts = Post.objects.all()
    if request.GET.get('filter', None) == 'drivers':
        posts = posts.filter(search_target=1)
    if request.GET.get('filter', None) == 'personal':
        posts = Post.objects.filter(author = request.user)

    paginator = Paginator(posts, 10)
    page = request.GET.get('page')
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)

    ctx = {'posts':posts, }
    return render(request, 'posts/list.html', {'posts':posts})

def post_detail(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    return render(request, 'posts/detail.html', {'post':post})

@login_required
def recommended(request):
    recommended_posts = []
    args_list = list(request.user.post_set.all().values())
    for args in args_list:
        recommended = Post.objects.filter(start_point__id=args['start_point_id'],
                                          end_point__id=args['end_point_id'],
                                          ).exclude(author=request.user)
        for item in list(recommended):
            recommended_posts.append(item)
        
    return render(request, 'posts/list.html', {'posts':recommended_posts})
