#!/usr/bin/python
# -*- coding: utf-8 -*-

import simplejson

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import HttpResponse, get_object_or_404
from django.shortcuts import render
from django.utils.translation import ugettext as _
from django.views.decorators.http import require_POST

from django_messages.views import reply, compose
from django_messages.forms import ComposeForm
from django_messages.utils import get_username_field

from apps.main.models import Post
from apps.main.utils import format_quote

def dashboard(request):
    passengers_posts = Post.objects.filter(search_target=0)[:3]
    drivers_posts = Post.objects.filter(search_target=1)[:3]
    ctx = {'passengers_posts':passengers_posts,
           'drivers_posts':drivers_posts, 
           }
    return render(request, 'dashboard.html', ctx)

@require_POST
def search(request):
    data = request.POST.copy()
    result = Post.objects.filter(search_target=data['search-target'],
                                 start_point__name__icontains = data['from'],
                                 end_point__name__icontains = data['to'],
                                 )

    paginator = Paginator(result, 2)
    page = request.GET.get('page')
    try:
        result = paginator.page(page)
    except PageNotAnInteger:
        result = paginator.page(1)
    except EmptyPage:
        result = paginator.page(paginator.num_pages)
    return render(request, 'posts/list.html', {'posts':result})

def autocomplete(request, node):
    term = request.GET.get('term')
    if node == "checkpoint":
        values = list(CheckPoint.objects.filter(name__icontains=term).distinct().values_list('name', flat=True))
        return HttpResponse(simplejson.dumps(values), mimetype='application/json')


@login_required
def message_reply(request, message_id):
    return reply(request, message_id, quote_helper=format_quote)

@login_required
def message_apply(request, post_id, recipient=None, form_class=ComposeForm,
                  template_name='django_messages/compose.html', success_url=None, recipient_filter=None):

    if request.method == "POST":
        sender = request.user
        form = form_class(request.POST, recipient_filter=recipient_filter)
        if form.is_valid():
            form.save(sender=request.user)
            messages.info(request, _(u"Message successfully sent."))
            if success_url is None:
                success_url = reverse('messages_inbox')
            if request.GET.has_key('next'):
                success_url = request.GET['next']
            return HttpResponseRedirect(success_url)
    else:
        post = get_object_or_404(Post, pk=post_id)
        recipient = post.author.username
        form = form_class()
        if recipient is not None:
            recipients = [u for u in User.objects.filter(**{'%s__in' % get_username_field(): [r.strip() for r in recipient.split('+')]})]
            form.fields['recipient'].initial = recipients
        form.fields['subject'].initial = u'Поездка {0}'.format(post)
        form.fields['body'].initial = u'Здравствуйте! \nЯ хочу поехать с вами по указанному маршруту.'
    return render(request, template_name, {'form': form,})
