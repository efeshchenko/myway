#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import User

SEARCH_TARGETS = (
    (0, 'Попутку'), 
    (1, 'Пассажира'), 
    )

GENDERS = (
    (0, 'Мужской'),
    (1, 'Женский')
)

ATTITUDES = (
    (0, 'Отрицательно'),
    (1, 'Нейтрально'),
    (2, 'Терпимо'),
)

TRIP_MODES = (
    (0, 'Регулярно'),
    (1, 'Один раз'),
)

class UserProfile(models.Model):
    user = models.OneToOneField(User)

    photo = models.ImageField('Фото', upload_to='static/upload/user_photos', blank=True, null=True)
    gender = models.IntegerField('Пол', choices=GENDERS, max_length=1, default=0)
    age = models.IntegerField('Возраст', blank=True, null=True)

    smoking = models.IntegerField('Оношение к курению', choices=ATTITUDES, default =1)
    music = models.CharField('Музыка', max_length=255, blank=True, null=True)

    phone = models.CharField('Телефон', blank=True, null=True, max_length=255)
    social_network_link = models.CharField('Ссылка на страницу в социальной сети', blank=True, null=True, max_length=255)
    skype_name = models.CharField('Skype', blank=True, null=True, max_length=255)
    icq =  models.CharField('ICQ', blank=True, null=True, max_length=255)

    about = models.TextField('Дополнительная информация', blank=True, null=True)

    class Meta:
        verbose_name='Профайл'
        verbose_name_plural='Профайлы'

    def __unicode__(self):
        return u'{0}({1})'.format(self.user.username, self.pk)

class Post(models.Model):

    search_target = models.IntegerField('Я ищу', max_length=1, choices=SEARCH_TARGETS, default=0 )
    start = models.CharField(db_index=True, max_length=255)
    end = models.CharField(db_index=True, max_length=255)
    # Для обьявлений о поиске пассажира
    places = models.IntegerField('Количество мест', null=True, blank=True)
    cost = models.DecimalField('Стоимость', max_digits=5, decimal_places=2, null=True, blank=True)
    car_info = models.CharField('Описание автомобиля', blank=True, null=True, max_length=255)

    trip_mode = models.IntegerField('График движения', choices=TRIP_MODES, max_length=1, default=0)
    single_departure = models.DateTimeField('Дата отправления (для разовой поездки)', null=True, blank=True)   
    regular_days = models.CharField('Дни недели (для регулярной)', max_length=255, null=True, blank=True)
    regular_departure = models.TimeField('Время отправления (для регулярной)', null=True, blank=True)

    additional_info = models.TextField('Доп инфо', blank=True, null=True)

    author = models.ForeignKey(User)
    created_on = models.DateTimeField(auto_now_add=True)
    edited_on = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created_on']
        verbose_name= 'Объявление'
        verbose_name_plural='Объявления'

    def __unicode__(self):
        display = u'{0} - {1}'.format(self.start.split(',')[0], self.end.split(',')[0])

        if self.trip_mode == 1:
            display += u' {0}'.format(self.single_departure)
        return display

def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.get_or_create(user=instance)
post_save.connect(create_user_profile, sender=User)


def __unicode__(self):
    if self.get_full_name():
        return self.get_full_name()
    else:
        return self.username

#User.add_to_class("__unicode__", __unicode__)
