#!/usr/bin/python
# -*- coding: utf-8 -*-

from django import template
from django.core.urlresolvers import resolve

register = template.Library()
@register.filter
def week_days_ru(value):
    value = value.replace('mon', u'Пн').replace('tue', u'Вт').replace('wed', u'Ср')\
        .replace('thu', u'Чт').replace('fri', u'Пт').replace('sat', u'Сб').replace('sun', u'Вс')
    return value

@register.simple_tag
def navactive(request, urls):
    current_url = resolve(request.path).url_name
    if current_url in urls.split():
        return "active"
    return ""
