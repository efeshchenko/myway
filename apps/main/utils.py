from django.utils.text import wrap
from django.utils.translation import ugettext

def format_quote(sender, body):
    
    lines = wrap(body, 55).split('\n')
    for i, line in enumerate(lines):
        lines[i] = "> %s" % line
    quote = '\n'.join(lines)
    return ugettext(u"%(sender)s wrote:\n%(body)s") % {
        'sender': sender.get_full_name(),
        'body': quote
    }
