#!/usr/bin/python
# -*- coding: utf-8 -*-

from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.forms.widgets import RadioSelect, TimeInput, PasswordInput, TextInput, SplitDateTimeWidget
from django.utils.translation import gettext_lazy as _

from apps.main.models import UserProfile, Post

class UserProfileForm(forms.ModelForm):
    first_name = forms.CharField(label=_('First Name'), max_length=30)
    last_name = forms.CharField(label=_('Last Name'), max_length=30)
    email = forms.CharField(label=_('email'), max_length=30)

    def __init__(self, *args, **kw):
        super(UserProfileForm, self).__init__(*args, **kw)
        self.fields['first_name'].initial = self.instance.user.first_name
        self.fields['last_name'].initial = self.instance.user.last_name
        self.fields['email'].initial = self.instance.user.email

    def save(self, *args, **kw):
        super(UserProfileForm, self).save(*args, **kw)
        self.instance.user.first_name = self.cleaned_data.get('first_name')
        self.instance.user.last_name = self.cleaned_data.get('last_name')
        self.instance.user.email = self.cleaned_data.get('email')
        self.instance.user.save()

    class Meta:
        model = UserProfile
        exclude = ('user',)

class PostForm(forms.ModelForm):

    class Meta:
        widgets = {
            'trip_type': RadioSelect(),
            'trip_mode': RadioSelect(),
            'search_target': RadioSelect(),
            'regular_departure': TimeInput(),
            'single_departure':SplitDateTimeWidget(),
            }
        model = Post

class CustomAuthForm(AuthenticationForm):
    username = forms.CharField(widget=TextInput(attrs={'placeholder': 'Логин'}))
    password = forms.CharField(widget=PasswordInput(attrs={'placeholder': 'Пароль'}))
