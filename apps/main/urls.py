from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns(
    'apps.main.views',
    url(r'^$', 'dashboard', name="dashboard"),

    url(r'^profile/(?P<profile_id>\d+)/$', 'profile.view_profile', name="view_profile"),
    url(r'^profile/edit/$', 'profile.update_profile', name="update_profile"),
    url(r'^profile/update_avatar/$', 'profile.update_avatar', name="update_avatar"),

    url(r'^posts/$', 'post.all_posts', name="all_posts"),
    url(r'^posts/recommended/$', 'post.recommended', name="recommended_posts"),
    url(r'^posts/(?P<post_id>\d+)/$', 'post.post_detail', name="post_detail"),
    url(r'^posts/add/$', 'post.post_form', name="post_add"),
    url(r'^posts/edit/(?P<post_id>\d+)/$', 'post.post_form', name="post_edit"),
    url(r'^posts/delete/(?P<post_id>\d+)/$', 'post.delete_post', name="post_delete"),

    url(r'^search/$', 'search', name="search"),
    url(r'^autocomplete/(?P<node>[-\w]+)/$', 'autocomplete', name="autocomplete"),
    )
