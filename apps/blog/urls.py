from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns(
    'apps.blog.views',
    url(r'^$', 'post_list', name="blog_post_list"),
    url(r'^post/(?P<post_id>\d+)/$', 'post_detail', name="blog_post_detail"),
)
