#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.db import models

from tinymce.models import HTMLField

class Post(models.Model):
    title = models.CharField('Заголовок', max_length=255)
    body = HTMLField('Пост' )
    published = models.BooleanField('Показать на сайте', default=False)
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name='Запись'
        verbose_name_plural='Записи'
        ordering = ['-created_on']

    def __unicode__(self):
        return u'{0} {1}'.format(self.title[:20], self.created_on)
