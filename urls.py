from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}), 
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^blog/', include('blog.urls')),
    url(r'^ulogin/', include('django_ulogin.urls')), 
    url('^faq/', include('fack.urls')), 
    url(r'^messages/reply/(?P<message_id>[\d]+)/$', 'apps.main.views.message_reply', name='messages_reply_custom'),
    url(r'^messages/apply/(?P<post_id>[\d]+)/$', 'apps.main.views.message_apply', name='messages_apply'),
    url(r'^messages/', include('django_messages.urls')),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^', include('apps.main.urls')),
)
