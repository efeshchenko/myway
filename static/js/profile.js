$(document).ready(function(){
    $('#image-form').ajaxForm({
        success:function(response){
            $('.profile_avatar').attr('src','/'+response);
        }
    });

    $('.delete_avatar').click(function(){
        var csrf = $('input[name="csrfmiddlewaretoken"]').attr('value');
        var data = {'clear_avatar':'true', 'csrfmiddlewaretoken': csrf};
        $.post('/profile/update_avatar/', data, function(response){
            $('.profile_avatar').attr('src','/'+response);
        })
        return false;
    })

    $('.change_avatar').click(function(){
        $('.hidden_fileinput').click();
        return false;
    })
    
    $('.hidden_fileinput').change(function(){
        $('#image-form').submit();
    });

    $('select').chosen({no_results_text: "Нет результатов"});
})