var init_interactive_radio = function(){
    $('#id_search_target_0').click(function(){$('.driver_field').hide('fast')});
    $('#id_search_target_1').click(function(){$('.driver_field').show('fast')});

    $('#id_trip_mode_0').click(function(){
        $('.trip-mode.single').hide();
        $('.trip-mode.regular').show();
    });
    $('#id_trip_mode_1').click(function(){
        $('.trip-mode.regular').hide();
        $('.trip-mode.single').show();
    })
}

function init_widgets(){
    $('select').chosen({no_results_text: "Нет результатов"});
    $('input[type="radio"]').ezMark();
    $('#id_regular_departure, #id_single_departure_1 ').timepicker({ 'timeFormat': 'H:i',
                                                                     'scrollDefaultNow': true
                                                                   });
    $('#id_single_departure_0').datepicker({
        dayName: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthNamesShort: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        dateFormat: 'dd.mm.yy',
        firstDay: 1
    });

    $('#id_start, #id_end').autocomplete({
        source:complete,
    });
    
    $('#id_single_departure_0').attr('placeholder', 'Дата');
    $('#id_single_departure_1').attr('placeholder', 'Время');
}

$('document').ready(function(){
    init_interactive_radio();
    init_widgets();
    $('input[checked="checked"]').click();
})