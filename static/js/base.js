function complete(request, response){//autocomplete with yandex api
    jQuery.ajaxSetup({async:false});
    result = [];
    $.get('http://geocode-maps.yandex.ru/1.x/?format=json&geocode='+request.term).success(function(response){
        $(response.response.GeoObjectCollection.featureMember).each(function(index, item){
            var text = item.GeoObject.metaDataProperty.GeocoderMetaData.text;
            text = text.split(',').reverse().join(",");
            text = text.trim();
            result.push(text);
        });
    });
    jQuery.ajaxSetup({async:true});
    response(result);
}