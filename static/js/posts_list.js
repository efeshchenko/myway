function ConfirmRemoveDialog(message, post_id){
    $('<div></div>').appendTo('body')
        .html('<div><h6>'+message+'?</h6></div>')
        .dialog({
            modal: true, 
            title: 'Удалить обьявление', 
            zIndex: 10000, 
            autoOpen: true,
            width: 'auto', 
            resizable: false,
            buttons: {
                Удалить: function(){
                    $.ajax({
                        beforeSend: function(xhr, settings){
                            var csrftoken = $.cookie('csrftoken');
                            xhr.setRequestHeader("X-CSRFToken", csrftoken);
                        },
                        url: '/posts/delete/'+post_id+'/',
                        type: 'DELETE',
                        success: function(result) {
                            $('#delete-'+post_id).parents('li').remove();
                        }
                    });
                    $(this).dialog("close");
                },
                Отмена: function () {
                    $(this).dialog("close");
                }
            },
            close: function (event, ui) {
                $(this).remove();
            }
        });
};

$(document).ready(function(){
    $('.delete-post').click(function(){
        var post_id = $(this).attr('id').split('delete-')[1];
        ConfirmRemoveDialog('Вы уверены', post_id);
        return false;
    });
})