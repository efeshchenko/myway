import os

DEBUG = False
TEMPLATE_DEBUG = DEBUG
PROJECT_PATH = os.path.abspath(os.path.dirname(__name__))

ADMINS = (
    ('pp admin', 'djangoway@gmail.com'),
)

MANAGERS = ADMINS

AUTH_PROFILE_MODULE = 'main.UserProfile'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.',
        'NAME': '', 
        'USER': '', 
        'PASSWORD': '', 
        'HOST': '', 
        'PORT': '', 
    }
}

TIME_ZONE = 'America/Chicago'
LANGUAGE_CODE = 'ru-Ru'
SITE_ID = 1
USE_I18N = True
USE_L10N = True
MEDIA_ROOT = ''
MEDIA_URL = ''
STATIC_ROOT = ''
STATIC_URL = '/static/'
ADMIN_MEDIA_PREFIX = '/static/admin/'

STATICFILES_DIRS = (
    os.path.join(PROJECT_PATH, 'static'), 
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

SECRET_KEY = 'g#z#z5lz%1b%bwbh@n!5qmijnj8bx6fis45494%jsz((&lj#45'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'myway.urls'

TEMPLATE_DIRS = (
    os.path.join(PROJECT_PATH, 'templates'), 
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth", 
    "django.core.context_processors.debug", 
    "django.core.context_processors.i18n", 
    "django.core.context_processors.media", 
    "django.core.context_processors.static", 
    "django.contrib.messages.context_processors.messages", 
    "django.core.context_processors.request", 
    "processors.loginform_processor", 
    )

INSTALLED_APPS = (
    #django core
    'django.contrib.auth', 
    'django.contrib.contenttypes', 
    'django.contrib.sessions', 
    'django.contrib.sites', 
    'django.contrib.messages', 
    'django.contrib.staticfiles', 
    'django.contrib.admin', 
    #3-rd party
    'admin_tools',
    'admin_tools.menu',
    'django_ulogin', 
    'django_messages', 
    'fack', 
    'registration', 
    'disqus',
    'tinymce',
    #home-made
    'main', 
    'ulogin_customization', 
    'blog',
)


#tinymce config
TINYMCE_DEFAULT_CONFIG = {
    'plugins': "table,spellchecker,paste,searchreplace",
    'theme': "advanced",
    'width':"60%",
    'height':"400px",
}

#disqus settings
DISQUS_API_KEY = 'vPPYM1roabIzK4Yghi9MPdx4XMDbgsVywoL0ZzofvxcHF8ovpp9YtqjdEnRL1poR'
DISQUS_WEBSITE_SHORTNAME = 'mywaycom'

#ulogin settings
ULOGIN_FIELDS = ['first_name', 'last_name', 'email']
ULOGIN_DISPLAY = 'button'

#registration settings
ACCOUNT_ACTIVATION_DAYS = 1

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

try:
    from settings_local import *
except ImportError:
    pass
